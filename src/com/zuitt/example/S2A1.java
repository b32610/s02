package com.zuitt.example;

import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args) {
        // Any year that is evenly divisible by 4 is a leap year
        // Confirm the number isn't evenly divisible by 100

        System.out.println("Input year to be checked if a leap year");
        Scanner yearScanner = new Scanner(System.in);
        System.out.println("Enter Year");

        int yearValue = yearScanner.nextInt() ;
        boolean bool = false;


        if (yearValue % 4 == 0) {

            if (yearValue % 100 == 0) {

                if (yearValue % 400 == 0)
                    bool = true;
                else
                    bool = false;
            }

            else
                bool = true;
        }

        else
            bool = false;

        if (bool)
            System.out.println(yearValue + " is a leap year.");
        else
            System.out.println(yearValue + " is NOT a leap year.");
    }
}
