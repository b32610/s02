package com.zuitt.example;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class Array {
    // [SECTION] Java Collection
        // Are a single units of objects
        // useful for manipulating relevant pieces of data that can be used in different situations,  more commonly with loops.

    public static void main(String[] args){
        /*
         [SECTION] Array
         in java, arrays are container of values of the same data type given a predefined amount of values.

         Arrays are more rigid, once the size and data type are defined, they can no longer change.
        */

        /*
          Syntax : Array Declaration
           datatype[] identifier = new datatype[numOfElements]
        */

        int[] intArray = new int[5];

        intArray[0] = 200;
        intArray[1] = 3;
        intArray[2] = 25;
        intArray[3] = 50;
        intArray[4] = 98;

        // This will return the memory address of the array.\
        System.out.println(intArray);
        // To print the intArray, we ned to import the Arrays class and use the .toString() method to convert the array to string
        System.out.println(Arrays.toString(intArray));

        // Syntax : Array declaration with initialization
            // Datatype[] identifier = {elementA, elementB, elementC, ... elementNth}

        String[] names = {"John", "Jane", "Joe"};
//         names[4] = "Joey";  out of bounds length error.

        System.out.println(Arrays.toString(names));

        // Sample java array methods :


        // Sort
        Arrays.sort((intArray));
        System.out.println("Order of the items after sort: " + Arrays.toString(intArray));

        // Multidimensional Arrays
            // A two-dimensional array can be described as two lengths of nested array within each other, like a matrix.
            // first length is row, second length is column.
            // Syntax : datatype[][] identifier = new String[column][row]

        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        System.out.println(Arrays.deepToString(classroom)); // Deep to string

        // ArrayLists
            // are resizeable arrays, wherein elements can be added or removed whenever it is needed.
            // Syntax : :
                // ArrayList<dataType> identifier = new ArrayList<datatype>()
        // dataType : objects
        // used to specify that the list can onl;y have one type of object in a collection
        // ArrayList - Cannot hold primitive data types "java wrapper classes" provide a way to use this types as object.

        // Declare an ArrayList
        ArrayList<String> students = new ArrayList<String>();

        // Add element
        // arrayListName.add(element);
        students.add("John");
        students.add("Paul");
        System.out.println(students);
        System.out.println(students.get(1));


        /*
            Adding an element on a specific index
                arrayListName.add(index, value);
        */

        students.add(1, "Mike");
        System.out.println(students);
        System.out.println(students.size()); // 3

        // Updating an element
        students.set(1,"George"); // [John, Mike, Paul]
        System.out.println(students); // [John, George, Paul]

        // Remove an element
        students.remove(1);
        System.out.println(students); // [John, Paul]

        // clear an element
        students.clear();
        System.out.println(students); // []

        // getting arraylist size
        // arrayList.size();
        System.out.println(students.size()); // 0

        // [SECTION] Hashmaps
        // most objects in Java are defined and are instantiations of Classes that contain a proper set of properties and methods.
        // There are might be use cases where is this not appropriate, or you may simply want to store a collection of data in key-value pairs
        // in Java "keys" also referred as "fields"
        // wherein the values are accessed by the fields
        // Syntax:
        // HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();

        // Declaring HashMaps

        HashMap<String, String> jobPosition = new HashMap<String, String>();

        // Add element
        // hashMapName.put(<field>, <value>);
        jobPosition.put("Student", "Brandon");
        jobPosition.put("Dreamer", "Alice");
//        jobPosition.put("Dreamer", "Alice"); the last added element with the same field will be overridden, whenever there are duplicate keys.

        System.out.println(jobPosition); // {Student=Brandon, Dreamer=Alice}

        // Access element
        // hashMapName.get("field");
        System.out.println(jobPosition.get("Dreamer"));

        // Updating the values
        // hashMapName.replace(keyToChanged, newValue);
        jobPosition.replace("Student", "Brandon Smith");
        System.out.println(jobPosition);

        // Remove an element
        // hashMapName.remove(key/field);
        jobPosition.remove("Dreamer");
        System.out.println(jobPosition);

        // Clear all the content
        // hashMapNames.clear();
        System.out.println(jobPosition);










    }
}

