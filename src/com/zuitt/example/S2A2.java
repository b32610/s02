package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class S2A2 {

    public static void main(String[] args){

        // 2, 3, 5, 7, and 11.

        int[] primeArray = new int[5];

        primeArray[0] = 2;
        primeArray[1] = 3;
        primeArray[2] = 5;
        primeArray[3] = 7;
        primeArray[4] = 11;

        // System.out.println(primeArray);
        // System.out.println(Arrays.toString(primeArray));
        System.out.println("The first prime number is : " + primeArray[0]);
        System.out.println("The second prime number is : " + primeArray[1]);
        System.out.println("The third prime number is : " + primeArray[2]);
        System.out.println("The fourth prime number is : " + primeArray[3]);
        System.out.println("The fifth prime number is : " + primeArray[4]);

        ArrayList<String> friends = new ArrayList<String>();
            friends.add("Dainiel");
            friends.add("Ara");
            friends.add("Alex");

            System.out.println("My friends are : " + friends);

        HashMap<String, Integer> inventory = new HashMap<String, Integer>();

            inventory.put("Toothpaste", 15);
            inventory.put("Toothbrush", 20);
            inventory.put("Soap", 12);

            System.out.println("Our current inventory consists of: " + inventory);




    }






}
